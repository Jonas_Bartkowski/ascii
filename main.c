#include <stdio.h>
#include <stdbool.h>

bool isPrintable(char c)
{
    return (c >= 32 && c <= 126);
}

void printChar(char c)
{
    printf("%03o ", c); //octal
    printf("%3d ", c); //decimal
    printf("%3x ", c); //hexadecimal
    if (isPrintable(c))
    {
        printf("%3c ", c);
    }
    else
    {
        printf("    ");
    }
}

void printHeader()
{
    for (int i = 0; i < 4; i++)
    {
        printf("Oct Dec Hex Chr ");
    }
    printf("Oct Dec Hex Chr\n");
    for (int i = 0; i < 4; i++)
    {
        printf("––––––––––––––– ");
    }
    printf("–––––––––––––––\n");
}

void adv()
{
    printHeader();
    for (int i = 0; i < 26; i++)
    {
        for (int j = i; j <= i+104; j+=26)
        {
            if (j < 128)
            {
                printChar(j);
            }
        }
        printf("\n");
    }
}

int old()
{
    printHeader();
    for (int i = 0; i <= 127; i++)
    {
        printChar(i);
        if (i % 5 == 0)
        {
            printf("\n");
        }
    }
    return 0;
}

int main()
{
    adv();
    return 0;
}
